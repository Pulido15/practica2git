import java.util.HashMap;

/**
 * Created by bartlomiej on 04.10.2016.
 */
public class Histogram <T> {
    HashMap<T, Integer> histogram = new HashMap<>();
    private final T[] vector;

    public Histogram(T[] vector) {
        this.vector = vector;

        for(T key: vector) {
            if (histogram.containsKey(key)) {
                histogram.put(key, histogram.get(key) + 1);
            } else {
                histogram.put(key, 1);
            }
        }
    }

    public HashMap<T, Integer> getHistogram() {
        return histogram;
    }
}
