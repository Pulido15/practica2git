import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        //int[] vector = {1, 3, 2, 1, 4, 5, 4, 7, 1, 3, 2};
        String[] vector = {"Ana", "Juan", "Pedro", "Ana", "Pedro"};
        Histogram hs = new Histogram(vector);
        HashMap<String, Integer> histogram = hs.getHistogram();

        for (String key : histogram.keySet()) {
            System.out.println(key + " --> " + histogram.get(key));
        }

        System.out.println("Hello World!");
    }
}
